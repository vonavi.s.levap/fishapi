create database fishes;
use fishes;
create table names (id int, name varchar(255));
insert into names (id, name) values (1,'fish1');
insert into names (id, name) values (2,'fish2');
insert into names (id, name) values (3,'fish3');
insert into names (id, name) values (4,'fish4');

create database fishnames;
use fishnames;
CREATE TABLE fishnames (id SERIAL PRIMARY KEY, fishname VARCHAR(255));
insert into fishnames (id, fishname) values (1,'fish1');
insert into fishnames (id, fishname) values (2,'fish2');
insert into fishnames (id, fishname) values (3,'fish3');
insert into fishnames (id, fishname) values (4,'fish4');
insert into fishnames (id, fishname) values (5,'fish5');
insert into fishnames (id, fishname) values (6,'fish6');