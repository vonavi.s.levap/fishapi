// This code was written based on the example from https://blog.logrocket.com/build-rest-api-node-express-mysql/

const express = require("express");
const app = express();
const port = 3000;
const fishdbrouter = require("./routes/fishapi");

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.get("/", (req, res) => {
  res.json({ message: "ok" });
});

app.use("/fishapi", fishdbrouter);

/* Error handler middleware*/
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({message: err.message});
  return;
});

app.listen(port, () => {
  console.log(`RMDB REST API app listening at http://localhost:${port}`);
});