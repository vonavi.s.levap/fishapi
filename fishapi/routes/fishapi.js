const express = require('express');
const router = express.Router();
const fishdb = require('../services/fishdb');
const fishjsondb = require('../services/fishjson')

/* GET fish names*/
router.get('/getfishnames', async function(req, res, next){
    try{
        res.json(await fishdb.getFishNames());
    } catch (err) {
        console.error('Error while getting fish names', err.message);
        next(err);
    }
});

/* GET fish name*/
router.get('/getfishname', async function(req, res, next){
    try{
        res.json(await fishdb.getFishName());
    } catch (err) {
        console.error('Error while getting fish name', err.message);
        next(err);
    }
});

/* GET fish names json*/
router.get('/getfishnamesjson', async function(req, res, next){
    try{
        res.json(await fishjsondb.getFishNames());
    } catch (err) {
        console.error('Error while getting fish names', err.message);
        next(err);
    }
});


module.exports = router;