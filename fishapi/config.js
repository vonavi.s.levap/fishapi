const config = {
    db: {
        host: "fishdb-service",
        port: "3306",
        user: "root",
        password: "",
        database: "fishes"
    }
};

module.exports = config;