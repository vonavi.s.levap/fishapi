
const http = require('http');

var options ={
    host: 'localhost',
    port: '3001',
    path: '/rmdb/getmacs/',
    method: 'get'
}

http.get(options, (resp) =>{
    let data = '';
    
    // A chunk of data has been received.
    resp.on('data', (chunk) => {
        data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
        console.log(data);
     });

}).on("error", (err) => {
  console.log("Error: " + err.message);
});