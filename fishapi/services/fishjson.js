var fs = require("fs");

async function getFishNames(){

    const data = fs.readFileSync( __dirname + "/../" + "fish.json", {encoding:'utf8', flag:'r'});
    return {data};
};

module.exports ={
    getFishNames
}