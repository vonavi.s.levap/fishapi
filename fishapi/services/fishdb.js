const db = require('./db');
const config = require('../config');

async function getFishNames(){

    const rows = await db.query('SELECT name FROM names;');
    return {rows};
};

async function getFishName(){

    const rows = await db.query("SELECT name FROM names WHERE id=1;");
    return {rows};
};

module.exports ={
    getFishNames,
    getFishName
}