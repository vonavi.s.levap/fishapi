use std::env;

use diesel::result::Error;
use rocket::http::Status;
use rocket::response::status;
use rocket_contrib::json::Json;

use crate::connection::DbConn;
use crate::repository;
use crate::model::FishName;
use crate::model::NewFishName;

#[get("/")]
pub fn all_fish_names(connection: DbConn) -> Result<Json<Vec<FishName>>, Status> {
    repository::show_fish_names(&connection)
        .map(|fishname| Json(fishname))
        .map_err(|error| error_status(error))
}

#[get("/<id>")]
pub fn get_fish_name(id: u64, connection: DbConn) -> Result<Json<FishName>, Status> {
    repository::get_fish_name(id, &connection)
        .map(|fishname| Json(fishname))
        .map_err(|error| error_status(error))
}

#[delete("/<id>")]
pub fn delete_fish_name(id: u64, connection: DbConn) -> Result<status::NoContent, Status> {
    repository::delete_fish_name(id, &connection)
        .map(|_| status::NoContent)
        .map_err(|error| error_status(error))
}

#[get("/retrieve_fish_name")]
pub fn retrieve_fish_name(connection: DbConn) -> Result<Json<Vec<FishName>>, Status> {
    repository::retrieve_fish_name(&connection)
        .map(|fishname| Json(fishname))
        .map_err(|error| error_status(error))
}

fn host() -> String {
    env::var("ROCKET_ADDRESS").expect("ROCKET_ADDRESS must be set")
}

fn port() -> String {
    env::var("ROCKET_PORT").expect("ROCKET_PORT must be set")
}

fn error_status(error: Error) -> Status {
    match error {
        Error::NotFound => Status::NotFound,
        _ => Status::InternalServerError
    }
}