use rocket;

use crate::connection;
use crate::handler;

pub fn create_routes() {
    rocket::ignite()
        .manage(connection::init_pool())
        .mount("/fish_names",
               routes![
                    handler::all_fish_names,
                    handler::get_fish_name,
                    handler::delete_fish_name,
                    handler::retrieve_fish_name
                    ],
        ).launch();
}