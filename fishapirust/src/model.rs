#![allow(proc_macro_derive_resolution_fallback)]

use crate::schema::fishnames;

#[derive(Queryable, AsChangeset, Serialize, Deserialize, Debug)]
#[table_name = "fishnames"]
pub struct FishName {
    pub id: u64,
    pub fishname:String
}

#[derive(Insertable, Serialize, Deserialize)]
#[table_name="fishnames"]
pub struct NewFishName {
    pub fishname: String
}