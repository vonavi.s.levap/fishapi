// @generated automatically by Diesel CLI.

diesel::table! {
    fishnames (id) {
        id -> Unsigned<Bigint>,
        fishname -> Varchar,
    }
}
