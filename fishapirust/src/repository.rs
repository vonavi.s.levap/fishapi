#![allow(proc_macro_derive_resolution_fallback)]

use diesel;
use diesel::prelude::*;

use crate::model::FishName;
use crate::model::NewFishName;

use crate::schema::fishnames;
use crate::schema::fishnames::dsl::*;

pub fn show_fish_names(connection: &MysqlConnection) -> QueryResult<Vec<FishName>>  {
    fishnames.limit(5)
       .load::<FishName>(connection)
}

pub fn get_fish_name(fish_id: u64, connection: &MysqlConnection) -> QueryResult<FishName> {
    fishnames::table.find(fish_id).get_result::<FishName>(connection)
}

pub fn delete_fish_name(fish_id: u64, connection: &MysqlConnection) -> QueryResult<usize> {
    diesel::delete(fishnames::table.find(fish_id))
        .execute(connection)
}

pub fn retrieve_fish_name(connection: &MysqlConnection) -> QueryResult<Vec<FishName>>  {
    let result = fishnames.limit(1)
       .load::<FishName>(connection);
    let result2 = fishnames.limit(1)
       .load::<FishName>(connection).unwrap();
    diesel::delete(fishnames::table.find(result2[0].id))
       .execute(connection);
    return Ok(result?);
}